
public class Sorted {
	public static boolean isSorted(int[] A){
		for (int i = 0; i < A.length - 1; ++i){
			if(A[i+1] < A[i])return false;
		}
		return true;
	}
}
