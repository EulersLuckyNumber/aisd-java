import java.util.Random;

public class QuickSort {
	private int swap = 0;
	private int comp = 0;
	private double[] swapTable;
	private double[] compTable;
	
	public void testSorting(int size, int repeat){
		swapTable = new double[size/100];
		compTable = new double[size/100];
		
		swap = 0;
		comp = 0;
		//int size = 1000;
		int range = 10000;
		Random generator = new Random();
		for(int l= 1; l <= size/100; ++l){
			int[] A = new int[l*100];
			//GENEROWANIE LOSOWEJ TABLICY
			swap = 0;
			comp = 0;
			for(int j = 0; j < repeat; ++j){
				for(int i = 0; i < A.length; ++i){
					A[i] = generator.nextInt(range);
				}
				
				sort(A, 0, A.length-1);
				swapTable[l-1] = (double)swap/(double)repeat;
				compTable[l-1] = (double)comp/(double)repeat;
			}
		}
		Chart chart = new Chart();
		chart.draw(swapTable, compTable, "QS");
		
		System.out.println();
		System.out.println("QS comp: " + Integer.toString(comp / repeat) + " for size: " + Integer.toString(size));
		System.out.println("QS swap: " + Integer.toString(swap / repeat) + " for size: " + Integer.toString(size));
		
		/*
		int[] A = new int[1000];
		for(int i = 0; i < A.length; ++i){
			A[i] = generator.nextInt(range);
		}
		sort(A, 0, A.length-1);
		*/
		
		
	}
	
	public void sort(int[] array, int left, int right){
		int piv = array[(left+right)/2];
		int i, j, temp;
		
		i = left;
		j = right;
		swap += 2;
		do{
			while (array[i] < piv){ 
				++i; 
				comp++;
			}
			while (array[j] > piv){
				--j; 
				comp++;
			}
			
			if(i <= j){
				temp = array[i];
				array[i] = array[j];
				array[j] = temp;
				swap += 3;
				++i;
				--j;
			}
			comp += 2;
		}while (i <= j);

		if(j > left) sort(array, left, j);
		if(i < right) sort(array, i, right);
	}
	
	public void reversedSorting(int size){
		swap = 0;
		comp = 0;
		int[] A = new int[size]; 
		for(int i = 0; i < size; ++i){
			A[i] = size - i;
		}
		sort(A, 0, A.length - 1);
		
		System.out.println();
		System.out.println("QS reversed comp: " + Integer.toString(comp) + " for size: " + Integer.toString(size));
		System.out.println("QS reversed swap: " + Integer.toString(swap) + " for size: " + Integer.toString(size));
	}
}
