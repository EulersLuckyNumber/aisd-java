import java.util.Random;

public class InsertionSort {
	private int swap = 0;
	private int comp = 0;
	private double[] swapTable;
	private double[] compTable;
	
	public void testSorting(int size, int repeat){
		swapTable = new double[size/100];
		compTable = new double[size/100];
		
		swap = 0;
		comp = 0;
		//int size = 1000;
		int range = 10000;
		Random generator = new Random();
		for(int l= 1; l <= size/100; ++l){
			int[] A = new int[l*100];
			//GENEROWANIE LOSOWEJ TABLICY
			swap = 0;
			comp = 0;
			for(int j = 0; j < repeat; ++j){
				for(int i = 0; i < A.length; ++i){
					A[i] = generator.nextInt(range);
				}
				
				sort(A);
				swapTable[l-1] = (double)swap/(double)repeat;
				compTable[l-1] = (double)comp/(double)repeat;
			}
		}
		Chart chart = new Chart();
		chart.draw(swapTable, compTable, "IS");
		
		System.out.println();
		System.out.println("IS comp: " + Integer.toString(comp / repeat) + " for size: " + Integer.toString(size));
		System.out.println("IS swap: " + Integer.toString(swap / repeat) + " for size: " + Integer.toString(size));
		
		
		//FRAGMENT DO TESTOWANIA POPRAWNOSCI SORTOWANIA
		/*
		for(int k = 0; k < A.length; ++k){
			System.out.println(A[k] + " ");
		}
		sort(A);

		System.out.println();
		for(int k = 0; k < A.length; ++k){
			System.out.println(A[k] + " ");
		}
		*/
	}
	
	public void reversedSorting(int size){
		swap = 0;
		comp = 0;
		int[] A = new int[size]; 
		for(int i = 0; i < size; ++i){
			A[i] = size - i;
		}
		sort(A);
		
		System.out.println();
		System.out.println("IS reversed comp: " + Integer.toString(comp) + " for size: " + Integer.toString(size));
		System.out.println("IS reversed swap: " + Integer.toString(swap) + " for size: " + Integer.toString(size));
	}
	
	public void sort(int[] A){
	
		int key;
		int j;
	
		for(int i = 1; i < A.length; ++i){
			
			key = A[i];
			j = i - 1;	
			swap += 2;
			
			while(j>=0 && A[j] > key){
				comp += 1;	
				
				A[j+1] = A[j];
				swap += 1;
				--j;
			}		
			A[j+1] = key;
			swap += 1;
		}
		//if(Sorted.isSorted(A)) System.out.println("success");
	}
	
}
