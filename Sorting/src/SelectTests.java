import java.util.Arrays;
import java.util.Random;

public class SelectTests {
	int size = 10000;
	boolean permutation = false;
	int repetitions = 20;
	int PS = 5;
	
	
	public void testSelect(){
		Select s = new Select();

		int[] A;
		double swap = 0;
		double comp = 0;
		
		int minComp = 0;
		int maxComp = 0;
		int minSwap = 0;
		int maxSwap = 0;
		
		for(int i = 0; i < repetitions; ++i){
			if(permutation)
				A = permArray(size);
			
			else
				A = randomArray(size);
			/*
			print(A);
			System.out.println("Statystyka pozycyjna nr: "+ Integer.toString(PS) + " to " + Integer.toString(s.selection(A, 0, A.length - 1, PS)));
			Arrays.sort(A);
			printV2(A, PS);
			*/
			s.selection(A, 0, A.length - 1, PS);
			swap += (double)s.swap / repetitions;
			comp += (double)s.comp / repetitions;
			
			if(s.swap > maxSwap || maxSwap == 0) maxSwap = s.swap;
			if(s.swap < minSwap || minSwap == 0) minSwap = s.swap;
			if(s.swap > maxComp || maxComp == 0) maxComp = s.comp;
			if(s.swap < minComp || minComp == 0) minComp = s.comp;
			
			s.swap = 0;
			s.comp = 0;

			//System.out.println();
		}
		System.out.println("avg swaps: " + Double.toString(swap));
		System.out.println("avg comps: " + Double.toString(comp));
		
		System.out.println("maxSwap: " + Double.toString(maxSwap));
		System.out.println("minSwap: " + Double.toString(minSwap));
		
		System.out.println("maxComp: " + Double.toString(maxComp));
		System.out.println("minComp: " + Double.toString(minComp));
		
		
	}
	
	public void testRandomSelect(){
		RandSelect s = new RandSelect();

		int[] A;
		double swap = 0;
		double comp = 0;
		
		int minComp = 0;
		int maxComp = 0;
		int minSwap = 0;
		int maxSwap = 0;
		
		for(int i = 0; i < repetitions; ++i){
			if(permutation)
				A = permArray(size);
			
			else
				A = randomArray(size);
			/*
			print(A);
			System.out.println("Statystyka pozycyjna nr: "+ Integer.toString(PS) + " to " + Integer.toString(s.selection(A, 0, A.length - 1, PS)));
			Arrays.sort(A);
			printV2(A, PS);
			*/
			s.selection(A, 0, A.length - 1, PS);
			
			swap += (double)s.swap / repetitions;
			comp += (double)s.comp / repetitions;
			
			if(s.swap > maxSwap || maxSwap == 0) maxSwap = s.swap;
			if(s.swap < minSwap || minSwap == 0) minSwap = s.swap;
			if(s.swap > maxComp || maxComp == 0) maxComp = s.comp;
			if(s.swap < minComp || minComp == 0) minComp = s.comp;
			
			s.swap = 0;
			s.comp = 0;

			//System.out.println();
		}
		System.out.println("avg swaps: " + Double.toString(swap));
		System.out.println("avg comps: " + Double.toString(comp));
		
		System.out.println("maxSwap: " + Double.toString(maxSwap));
		System.out.println("minSwap: " + Double.toString(minSwap));
		
		System.out.println("maxComp: " + Double.toString(maxComp));
		System.out.println("minComp: " + Double.toString(minComp));
		
		
	}
	
	
	
	private int[] randomArray(int size){
		Random random = new Random();
		int[] temp = new int[size];
		for(int i = 0; i < size; ++i){
			temp[i] = random.nextInt(10);
		}
		
		return temp;
	}
	
	private int[] permArray(int size){
		Random random = new Random();
		int[] temp = new int[size];
		for(int i = 0; i < size; ++i){
			temp[i] = i + 1;
		}
		for(int j = 0; j < size; ++j){
			swap(temp, j, random.nextInt(size - 1));
		}
		return temp;
	}
	
	public void swap(int[] A, int x, int y){
		int temp = A[x];
		A[x] = A[y];
		A[y] = temp;
	}
	
	public void print(int A[]){
    	
        for (int i=0; i<A.length; ++i)
            System.out.print(A[i]+" ");
        System.out.println();
    }
	
	public void printV2(int A[], int PS){
    	
        for (int i=0; i<A.length; ++i){
        	if(i == PS - 1){
        		System.out.print("( ");
        	}
            System.out.print(A[i]+" ");
            if(i == PS - 1){
            	System.out.print(")");
            }
        }
        System.out.println();
    }
}
