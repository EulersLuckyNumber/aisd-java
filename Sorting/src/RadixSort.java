import java.util.*;
 
public class RadixSort {
	
	private int comp = 0;
	private int swap = 0;
	private double[] swapTable;
	private double[] compTable;
	
	public void testSorting(int size, int repeat){
		
		swapTable = new double[size/100];
		compTable = new double[size/100];
		
		swap = 0;
		comp = 0;
		//int size = 1000;
		int range = 1000;
		Random generator = new Random();
		
		for(int l= 1; l <= size/100; ++l){
			int[] A = new int[l*100];
			//GENEROWANIE LOSOWEJ TABLICY
			swap = 0;
			comp = 0;
			for(int j = 0; j < repeat; ++j){
				for(int i = 0; i < A.length; ++i){
					A[i] = generator.nextInt(range);
				}
				
				sort(A, A.length);
				swapTable[l-1] = (double)swap/(double)repeat;
				compTable[l-1] = (double)comp/(double)repeat;
			}
		}
		Chart chart = new Chart();
		chart.draw(swapTable, compTable, "radix");
			System.out.println();
			System.out.println("Radix comp: " + Integer.toString(comp / repeat) + " for size: " + Integer.toString(size));
			System.out.println("Radix swap: " + Integer.toString(swap / repeat) + " for size: " + Integer.toString(size));
		
		
		
		/*
		//FRAGMENT DO TESTOWANIA POPRAWNOSCI SORTOWANIA
		int[] A = new int[100];
		for(int i = 0; i < A.length; ++i){
			A[i] = generator.nextInt(range);
		}
		for(int k = 0; k < A.length; ++k){
			System.out.println(A[k] + " ");
		}
		System.out.println();
		sort(A, A.length);
		
		for(int k = 0; k < A.length; ++k){
			System.out.println(A[k] + " ");
		}	
		*/
	}
	
	
    void sort(int A[], int n){
        int m = getMax(A);
        swap++;
 
        for (int exp = 1; m/exp > 0; exp *= 10)
            countSort(A, n, exp);
    }
    
    void countSort(int A[], int n, int exp){
    	
        int result[] = new int[n];
        int i;
        int count[] = new int[10];
        Arrays.fill(count,0);
 
        for (i = 0; i < n; i++){
            count[ (A[i]/exp)%10 ]++;
        }
        
        for (i = 1; i < 10; i++){
        	swap++;
            count[i] += count[i - 1];
        }
        

        for (i = n - 1; i >= 0; i--){
        	swap++;
            result[count[ (A[i]/exp)%10 ] - 1] = A[i];
            count[ (A[i]/exp)%10 ]--;
        }

        for (i = 0; i < n; i++){
        	swap++;
            A[i] = result[i];
        }
    }
 
    void print(int A[]){
    	
        for (int i=0; i<A.length; i++)
            System.out.print(A[i]+" ");
    }
    
    int getMax(int A[]){
    	
    	swap++;
        int mx = A[0];
        for (int i = 1; i < A.length; i++){
        	comp++;
            if (A[i] > mx)
                mx = A[i];
        }
        return mx;
    }
    
    public void reversedSorting(int size){
		swap = 0;
		comp = 0;
		int[] A = new int[size]; 
		for(int i = 0; i < size; ++i){
			A[i] = size - i;
		}
		sort(A, A.length);
		
		System.out.println();
		System.out.println("radix reversed comp: " + Integer.toString(comp) + " for size: " + Integer.toString(size));
		System.out.println("radix reversed swap: " + Integer.toString(swap) + " for size: " + Integer.toString(size));
	}
}
