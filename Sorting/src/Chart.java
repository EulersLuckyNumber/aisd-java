import com.objectplanet.chart.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Chart {
	
	public void draw(double[] data, double[] data2, String title){
		
		LineChart chart = new LineChart();
		
		String[] label = new String[data.length];
		
		for(int i = 0; i < data.length; i += data.length/10){			
			label[i] = Integer.toString((i * 100));
		}
		label[0] = Integer.toString(100);
		chart.setSeriesCount(2);
	    chart.setSampleCount(data.length);
	    chart.setSampleValues(0, data);
	    chart.setSampleValues(1, data2);
	    chart.setSampleLabels(label);
	    chart.setSampleLabelsOn(true);
	    chart.setRange(0, data[data.length - 1]);
	    chart.setSeriesLabel(0, "swaps");
	    chart.setSeriesLabel(1, "compares");
	    chart.setLegendOn(true);
	    Frame f = new Frame();
	    f.setTitle(title);
	    f.addWindowListener(new WindowAdapter() {
	        public void windowClosing(WindowEvent we) {
	            System.exit(0);
	        }
	    	}
	    );
	    f.add("Center", chart);
	    f.setSize(700,700);
	    f.show();
	}
}
