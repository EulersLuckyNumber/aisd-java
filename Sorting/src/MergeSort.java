import java.util.Random;

public class MergeSort {
	private int swap = 0;
	private int comp = 0;
	private double[] swapTable;
	private double[] compTable;
	
	public void testSorting(int size, int repeat){
		swapTable = new double[size/100];
		compTable = new double[size/100];
		
		swap = 0;
		comp = 0;
		//int size = 1000;
		int range = 10000;
		Random generator = new Random();
		for(int l= 1; l <= size/100; ++l){
			int[] A = new int[l*100];
			//GENEROWANIE LOSOWEJ TABLICY
			swap = 0;
			comp = 0;
			for(int j = 0; j < repeat; ++j){
				for(int i = 0; i < A.length; ++i){
					A[i] = generator.nextInt(range);
				}
				
				sort(A, 0, A.length-1);
				swapTable[l-1] = (double)swap/(double)repeat;
				compTable[l-1] = (double)comp/(double)repeat;
			}
		}
		Chart chart = new Chart();
		chart.draw(swapTable, compTable, "MS");
		System.out.println();
		System.out.println("MS comp: " + Integer.toString(comp / repeat) + " for size: " + Integer.toString(size));
		System.out.println("MS swap: " + Integer.toString(swap / repeat) + " for size: " + Integer.toString(size));
		
		
		//FRAGMENT DO TESTOWANIA POPRAWNOSCI SORTOWANIA
		/*
		for(int k = 0; k < array.length; ++k){
			System.out.print(array[k] + " ");
		}
		
		array = sort(array, 0, array.length-1);
		
		System.out.println();
		for(int k = 0; k < array.length; ++k){
			System.out.print(array[k] + " ");
		}
		*/
	}
	
	public int[] sort(int[] array, int p, int r){
		if(p == r){
			int[] temp = new int[1];
			temp[0] = array[p];
			swap++;
			return temp;		
		}
		else{
			int q = (p+r)/2;
			
			int[] A = sort(array, p, q);
			int[] B = sort(array, q+1, r);
			
			int[] C =  merge(A, B);
			//if(Sorted.isSorted(C)) System.out.println("success");
			return C;
		}
		
		
	}
	
	private int[] merge(int[] A, int[] B){
		int i = 0;
		int j = 0;
		int[] C = new int[A.length + B.length];
		
		while(i != A.length && j != B.length){
			if(A[i] < B[j]){
				comp++;
				C[i+j] = A[i];
				swap++;
				++i;
			}
			else{
				comp++;
				C[i+j] = B[j];
				swap++;
				++j;
			}			
		}
		
		if(i == A.length){
			while(j != B.length){
				C[i+j] = B[j];
				swap++;
				++j;
			}
		}
		if(j == B.length){
			while(i != A.length){
				C[i+j] = A[i];
				swap++;
				++i;
			}
		}
		return C;
	}
	
	public void reversedSorting(int size){
		swap = 0;
		comp = 0;
		int[] A = new int[size]; 
		for(int i = 0; i < size; ++i){
			A[i] = size - i;
		}
		sort(A, 0, A.length - 1);
		
		System.out.println();
		System.out.println("MS reversed comp: " + Integer.toString(comp) + " for size: " + Integer.toString(size));
		System.out.println("MS reversed swap: " + Integer.toString(swap) + " for size: " + Integer.toString(size));
	}

}
