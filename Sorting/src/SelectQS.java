import java.util.Random;

public class SelectQS {
	private int swap = 0;
	private int comp = 0;
	private double[] swapTable;
	private double[] compTable;
	
	public void testSorting(int size, int repeat){
		swapTable = new double[size/100];
		compTable = new double[size/100];
		
		swap = 0;
		comp = 0;
		//int size = 1000;
		int range = 10000;
		Random generator = new Random();
		for(int l= 1; l <= size/100; ++l){
			int[] A = new int[l*100];
			//GENEROWANIE LOSOWEJ TABLICY
			swap = 0;
			comp = 0;
			for(int j = 0; j < repeat; ++j){
				for(int i = 0; i < A.length; ++i){
					A[i] = generator.nextInt(range);
				}
				
				sort(A, 0, A.length-1);
				swapTable[l-1] = (double)swap/(double)repeat;
				compTable[l-1] = (double)comp/(double)repeat;
			}
		}
		Chart chart = new Chart();
		chart.draw(swapTable, compTable, "SQS");
		
		System.out.println();
		System.out.println("SQS comp: " + Integer.toString(comp / repeat) + " for size: " + Integer.toString(size));
		System.out.println("SQS swap: " + Integer.toString(swap / repeat) + " for size: " + Integer.toString(size));
		
		/*
		int[] A = new int[1000];
		for(int i = 0; i < A.length; ++i){
			A[i] = generator.nextInt(range);
		}
		sort(A, 0, A.length-1);
		*/
		
		
	}
	
	public void sort(int[] array, int left, int right){
		int piv = findMedian(array, left, right - 1);
		int i, j, temp;
		
		i = left;
		j = right;
		swap += 2;
		do{
			while (array[i] < piv){ 
				++i; 
				comp++;
			}
			while (array[j] > piv){
				--j; 
				comp++;
			}
			
			if(i <= j){
				temp = array[i];
				array[i] = array[j];
				array[j] = temp;
				swap += 3;
				++i;
				--j;
			}
			comp += 2;
		}while (i <= j);

		if(j > left) sort(array, left, j);
		if(i < right) sort(array, i, right);
	}
	
	public void reversedSorting(int size){
		swap = 0;
		comp = 0;
		int[] A = new int[size]; 
		for(int i = 0; i < size; ++i){
			A[i] = size - i;
		}
		sort(A, 0, A.length - 1);
		
		System.out.println();
		System.out.println("QS reversed comp: " + Integer.toString(comp) + " for size: " + Integer.toString(size));
		System.out.println("QS reversed swap: " + Integer.toString(swap) + " for size: " + Integer.toString(size));
	}
	
	public int findMedian(int[] B, int start, int end){
		int[] A = new int[end - start + 1];
		for(int k = 0; k < A.length; ++k){
			A[k] = B[start + k];
			swap++;
		}
		
		int a = A.length / 5;
		int[] medians;
		comp++;
		if(A.length % 5 != 0){
			medians = new int[a + 1];
		}
		else{
			medians = new int[a];
		}
		
		int[] temp = new int[5];
		int p = 0;

		for(int i = 0; i < a; ++i){
			for(int j = 0; j < 5; ++j){
				swap++;
				temp[j] = A[p];
				++p;
			}
			sort(temp);
			medians[i] = temp[2];
			swap++;
		}
		comp++;
		if(A.length % 5 != 0){
			int[] t = new int[A.length % 5];
			for(int j = 0; j < t.length; ++j){
				swap++;
				t[j] = A[A.length - j - 1];
			}
			sort(t);
			//System.out.println("medians:");
			swap++;
			medians[a] = t[t.length / 2];
		}
		//print(medians);
		comp++;
		if(medians.length > 5)
			return findMedian(medians, 0, medians.length - 1);
		else{
			swap++;
			sort(medians);			
			return medians[medians.length / 2];
		}
	}
	
public void sort(int[] A){
		
		int key;
		int j;
	
		for(int i = 1; i < A.length; ++i){
			
			key = A[i];
			j = i - 1;	
			
			while(j>=0 && A[j] > key){				
				A[j+1] = A[j];
				--j;
			}		
			A[j+1] = key;
		}
	}
	
	public void print(int A[]){
		
        for (int i=0; i<A.length; i++)
            System.out.print(A[i]+" ");
        System.out.println();
    }
}
