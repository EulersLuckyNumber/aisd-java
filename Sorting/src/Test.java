
public class Test {

	public static void main(String[] args) {

		float start;
		float stop;
		int size = 10000;
		int repetitions = 100;
		
		//for(int k = 1; k < 10; k++){
		//Rand.randomSelect(A, 6);	
		//}
		
		/*
		SelectTests ST = new SelectTests();

		start = System.nanoTime();
		ST.testSelect();
		stop = System.nanoTime();
		System.out.println("Select time:  " + Float.toString((stop - start)/1000000000) + " sec");
		
		start = System.nanoTime();
		ST.testRandomSelect();
		stop = System.nanoTime();
		System.out.println("Rand Select time:  " + Float.toString((stop - start)/1000000000) + " sec");
		/*
		
		
		
		RadixSort RS = new RadixSort();
		RS.testSorting(size, repetitions);
		RS.reversedSorting(size);
		
		/*
		InsertionSort IS = new InsertionSort();
		start = System.nanoTime();
		IS.testSorting(size, repetitions);
		stop = System.nanoTime();
		System.out.println("IS time:  " + Float.toString((stop - start)/1000000000) + " sec");
		
		MergeSort MS = new MergeSort();
		start = System.nanoTime();
		MS.testSorting(size, repetitions);
		stop = System.nanoTime();
		System.out.println("MS time:  " + Float.toString((stop - start)/1000000000) + " sec");
		
		
		IMSort IMS = new IMSort();
		start = System.nanoTime();
		IMS.testSorting(size, repetitions);
		stop = System.nanoTime();
		System.out.println("IMS time:  " + Float.toString((stop - start)/1000000000) + " sec");
		
		*/
		QuickSort QS = new QuickSort();
		start = System.nanoTime();
		QS.testSorting(size, repetitions);
		stop = System.nanoTime();
		System.out.println("QS time:  " + Float.toString((stop - start)/1000000000) + " sec");
		
		SelectQS SQS = new SelectQS();
		start = System.nanoTime();
		SQS.testSorting(size, repetitions);
		stop = System.nanoTime();
		System.out.println("SQS time:  " + Float.toString((stop - start)/1000000000) + " sec");
		/*
		IQSort IQS = new IQSort();
		start = System.nanoTime();
		IQS.testSorting(size, repetitions);
		stop = System.nanoTime();
		System.out.println("IQS time:  " + Float.toString((stop - start)/1000000000) + " sec");
		
		
		DPQuickSort DPQS = new DPQuickSort();
		start = System.nanoTime();
		DPQS.testSorting(size, repetitions);
		stop = System.nanoTime();
		System.out.println("DPQS time:  " + Float.toString((stop - start)/1000000000) + " sec");
		

		
		
		MS.reversedSorting(size);
		IQS.reversedSorting(size);
		IS.reversedSorting(size);
		QS.reversedSorting(size);
		IMS.reversedSorting(size);
*/
		
	}

}
