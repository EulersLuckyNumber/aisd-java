
public class Select {
	
	public int swap = 0;
	public int comp = 0;
	
	public int findMedian(int[] B, int start, int end){
		int[] A = new int[end - start + 1];
		for(int k = 0; k < A.length; ++k){
			A[k] = B[start + k];
			swap++;
		}
		
		int a = A.length / 5;
		int[] medians;
		comp++;
		if(A.length % 5 != 0){
			medians = new int[a + 1];
		}
		else{
			medians = new int[a];
		}
		
		int[] temp = new int[5];
		int p = 0;

		for(int i = 0; i < a; ++i){
			for(int j = 0; j < 5; ++j){
				swap++;
				temp[j] = A[p];
				++p;
			}
			sort(temp);
			medians[i] = temp[2];
			swap++;
		}
		comp++;
		if(A.length % 5 != 0){
			int[] t = new int[A.length % 5];
			for(int j = 0; j < t.length; ++j){
				swap++;
				t[j] = A[A.length - j - 1];
			}
			sort(t);
			//System.out.println("medians:");
			swap++;
			medians[a] = t[t.length / 2];
		}
		//print(medians);
		comp++;
		if(medians.length > 5)
			return findMedian(medians, 0, medians.length - 1);
		else{
			swap++;
			sort(medians);			
			return medians[medians.length / 2];
		}
	}
	

	
	public void sort(int[] A){
		
		int key;
		int j;
	
		for(int i = 1; i < A.length; ++i){
			
			key = A[i];
			j = i - 1;	
			
			while(j>=0 && A[j] > key){				
				A[j+1] = A[j];
				--j;
			}		
			A[j+1] = key;
		}
	}
	
	public void print(int A[]){
		
        for (int i=0; i<A.length; i++)
            System.out.print(A[i]+" ");
        System.out.println();
    }
	

	public int partition(int[] arr, int start, int end){
		int pivot = findMedian(arr, start, end);

	    int pivotIdx = 0;
	    while(arr[pivotIdx] != pivot){
	    	pivotIdx++;
	    }

	    swap(arr, pivotIdx, end);
	    pivotIdx = end;
	   
	    int i = start -1;
	    swap += 4;
	    for(int j=start; j<=end-1; ++j){
	    	comp++;
	        if(arr[j] <= pivot){
	            i = i+1;
	            swap(arr,i, j);
	        }
	    }

	    swap(arr, i+1, pivotIdx);
	    return i+1;
	}

	 

	public int selection(int[] arr, int start, int end, int k){

		comp +=3;
		if(start == end)
	        return arr[start];
	    if(k <= 0) 
	    	return -1;
	    if(start < end){
	    	swap++;
	    	int mid = partition(arr, start, end);
	    	int i = mid - start + 1;
	    	swap++;
	    	comp += 3;
	    	if(i == k)
	    		return arr[mid];
	    	else if(k < i)
	    		return selection(arr, start, mid-1, k);
	    	else 
	    		return selection(arr, mid+1, end, k-i);
	    }
	    
	    return 0;
	}
	
	public void swap(int[] A, int x, int y){
		int temp = A[x];
		A[x] = A[y];
		A[y] = temp;
		swap += 3;
	}

}
