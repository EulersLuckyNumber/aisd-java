import java.util.Random;

public class DPQuickSort {
	
	private int swap = 0;
	private int comp = 0;
	private double[] swapTable;
	private double[] compTable;
	
	public void testSorting(int size, int repeat){
		swapTable = new double[size/100];
		compTable = new double[size/100];
		
		swap = 0;
		comp = 0;
		//int size = 1000;
		int range = 10000;
		Random generator = new Random();
		for(int l= 1; l <= size/100; ++l){
			int[] A = new int[l*100];
			//GENEROWANIE LOSOWEJ TABLICY
			swap = 0;
			comp = 0;
			for(int j = 0; j < repeat; ++j){
				for(int i = 0; i < l; ++i){
					A[i] = generator.nextInt(range);
				}
				
				sort(A, 0, A.length-1);
				swapTable[l-1] = (double)swap/(double)repeat;
				compTable[l-1] = (double)comp/(double)repeat;
			}
		}
		Chart chart = new Chart();
		chart.draw(swapTable, compTable, "DPQS");
		
		System.out.println();
		System.out.println("DPQS comp: " + Integer.toString(comp / repeat) + " for size: " + Integer.toString(size));
		System.out.println("DPQS swap: " + Integer.toString(swap / repeat) + " for size: " + Integer.toString(size));
		
		
		/*
		//FRAGMENT DO TESTOWANIA POPRAWNOSCI SORTOWANIA
		Random generator = new Random();
		int[] A = new int[1000];
		for(int i = 0; i < 1000; ++i){
			A[i] = generator.nextInt(1000);
		}
//		for(int k = 0; k < A.length; ++k){
	//		System.out.println(A[k] + " ");
		//}
		sort(A,0, A.length);
		
		System.out.println();
		for(int k = 0; k < A.length; ++k){
			System.out.println(A[k] + " ");
		}
		
		if(Sorted.isSorted(A)) System.out.println("success");
		else System.out.println("fail");
		
		*/
	}
	
	public  void sort(int[] a) {
	    sort(a, 0, a.length);
	}
	public  void sort(int[] a, int fromIndex, int toIndex) {
	    rangeCheck(a.length, fromIndex, toIndex);
	    dualPivotQuicksort(a, fromIndex, toIndex - 1, 3);
	}
	private  void rangeCheck(int length, int fromIndex, int toIndex) {
	    if (fromIndex > toIndex) {
	        throw new IllegalArgumentException("fromIndex > toIndex");
	    }
	    if (fromIndex < 0) {
	        throw new ArrayIndexOutOfBoundsException(fromIndex);
	    }
	    if (toIndex > length) {
	        throw new ArrayIndexOutOfBoundsException(toIndex);
	    }
	}
	private void swap(int[] a, int i, int j) {
	    int temp = a[i];
	    a[i] = a[j];
	    a[j] = temp;
	}
	private void dualPivotQuicksort(int[] a, int left,int right, int div) {
	    int len = right - left;
	    if (len < 27) { // insertion sort for tiny array
	        for (int i = left + 1; i <= right; i++){
	            for (int j = i; j > left && a[j] < a[j - 1]; j--) {
	                swap(a, j, j - 1);
	            }
	        }
	        return;
	    }
	    int third = len / div;
	    // "medians"
	    int m1 = left  + third;
	    int m2 = right - third;
	    if (m1 <= left) {
	    	m1 = left + 1;
	    }
	    if (m2 >= right) {
	        m2 = right - 1;
	    }
	    if (a[m1] < a[m2]) {
	        swap(a, m1, left);
	        swap(a, m2, right);
	    }
	    else {
	        swap(a, m1, right);
	        swap(a, m2, left);
	    }
	    // pivots
	    int pivot1 = a[left];
	    int pivot2 = a[right];
	    // pointers
	    int less  = left  + 1;
	    int great = right - 1;
	    // sorting
	    for (int k = less; k <= great; k++) {
	        if (a[k] < pivot1) {
	            swap(a, k, less++);
	        } 
	        else if (a[k] > pivot2) {
	            while (k < great && a[great] > pivot2) {
	                great--;
	            }
	            swap(a, k, great--);
	            if (a[k] < pivot1) {
	                swap(a, k, less++);
	            }
	        }
	    }
	    // swaps
	    int dist = great - less;
	    if (dist < 13) {
	       div++;
	    }
	    swap(a, less  - 1, left);
	    swap(a, great + 1, right);
	    // subarrays
	    dualPivotQuicksort(a, left,   less - 2, div);
	    dualPivotQuicksort(a, great + 2, right, div);
	    // equal elements
	    if (dist > len - 13 && pivot1 != pivot2) {
	        for (int k = less; k <= great; k++) {
	            if (a[k] == pivot1) {
	                swap(a, k, less++);
	            }
	            else if (a[k] == pivot2) {
	                swap(a, k, great--);
	                if (a[k] == pivot1) {
	                    swap(a, k, less++);
	                }
	            }
	        }
	    }
	    // subarray
	    if (pivot1 < pivot2) {
	        dualPivotQuicksort(a, less, great, div);
	    }

	        }
	
	


}
