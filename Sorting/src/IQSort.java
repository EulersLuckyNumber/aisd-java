import java.util.Random;

public class IQSort {
	private int swap = 0;
	private int comp = 0;
	private double[] swapTable;
	private double[] compTable;
	
	public void testSorting(int size, int repeat){
		swapTable = new double[size/100];
		compTable = new double[size/100];
		
		swap = 0;
		comp = 0;
		//int size = 1000;
		int range = 10000;
		Random generator = new Random();
		for(int l= 1; l <= size/100; ++l){
			int[] A = new int[l*100];
			//GENEROWANIE LOSOWEJ TABLICY
			swap = 0;
			comp = 0;
			for(int j = 0; j < repeat; ++j){
				for(int i = 0; i < A.length; ++i){
					A[i] = generator.nextInt(range);
				}
				
				sort(A, 0, A.length-1);
				swapTable[l-1] = (double)swap/(double)repeat;
				compTable[l-1] = (double)comp/(double)repeat;
			}
		}
		Chart chart = new Chart();
		chart.draw(swapTable, compTable, "IQS");
		System.out.println();
		System.out.println("IQS comp: " + Integer.toString(comp / repeat) + " for size: " + Integer.toString(size));
		System.out.println("IQS swap: " + Integer.toString(swap / repeat) + " for size: " + Integer.toString(size));
		
		
		//FRAGMENT DO TESTOWANIA POPRAWNOSCI SORTOWANIA
		/*
		for(int k = 0; k < array.length; ++k){
			System.out.println(array[k] + " ");
		}
		sort(array, 0, array.length-1);

		System.out.println();
		for(int k = 0; k < array.length; ++k){
			System.out.println(array[k] + " ");
		}
		*/
	}
	
	public void sort(int[] array, int left, int right){
		int piv = array[(left+right)/2];
		int i, j, x;
		
		i = left;
		j = right;
		swap += 2;
		do{
			while (array[i] < piv)
			{ 
				++i; 
				comp++;
				}
			while (array[j] > piv){ --j; comp++;}
			
			if(i <= j){
				x = array[i];
				array[i] = array[j];
				array[j] = x;
				swap += 3;
				++i;
				--j;
			}
			comp += 2;
		}while (i <= j);

		if(j > left){
			if(j - left < 10){
				sortInsert(array, left, j);
			}
			else{
				sort(array, left, j);
			}
		}
		if(i < right){
			if(right - i < 10){
				sortInsert(array, i, right);
			}
			else{
				sort(array, i, right);
			}
		}
	}
	
	public void sortInsert(int[] A, int p, int r){
		int key;
		int j;
		
		for(int i = p; i < r + 1; ++i){
			
			key = A[i];
			j = i - 1;	
			swap += 2;
			
			while(j>=0 && A[j] > key){
				comp += 1;	
				
				A[j+1] = A[j];
				swap += 1;
				--j;
			}		
			A[j+1] = key;
			swap += 1;
		}
	}
	
	public void reversedSorting(int size){
		swap = 0;
		comp = 0;
		int[] A = new int[size]; 
		for(int i = 0; i < size; ++i){
			A[i] = size - i;
		}
		sort(A, 0, A.length - 1);
		
		System.out.println();
		System.out.println("IQS reversed comp: " + Integer.toString(comp) + " for size: " + Integer.toString(size));
		System.out.println("IQS reversed swap: " + Integer.toString(swap) + " for size: " + Integer.toString(size));
	}
}
