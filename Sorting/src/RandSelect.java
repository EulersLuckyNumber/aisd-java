
public class RandSelect {

	public int swap = 0;
	public int comp = 0;
	
	public int partition(int[] arr, int start, int end){
	    int pivotIdx = start;
	    int pivot = arr[pivotIdx];
	    swap(arr, pivotIdx, end);
	    pivotIdx = end;
	   
	    int i = start -1;
	    swap += 4;
	    for(int j=start; j<=end-1; ++j){
	    	comp++;
	        if(arr[j] <= pivot){
	            i = i+1;
	            swap(arr,i, j);
	        }
	    }

	    swap(arr, i+1, pivotIdx);
	    return i+1;
	}

	 

	public int selection(int[] arr, int start, int end, int k){

		comp +=3;
		if(start == end)
	        return arr[start];
	    if(k <= 0) 
	    	return -1;
	    if(start < end){
	    	swap++;
	    	int mid = partition(arr, start, end);
	    	int i = mid - start + 1;
	    	swap++;
	    	comp += 3;
	    	if(i == k)
	    		return arr[mid];
	    	else if(k < i)
	    		return selection(arr, start, mid-1, k);
	    	else 
	    		return selection(arr, mid+1, end, k-i);
	    }
	    
	    return 0;
	}
	
	public void swap(int[] A, int x, int y){
		int temp = A[x];
		A[x] = A[y];
		A[y] = temp;
		swap += 3;
	}
	
	public void print(int A[], int n){
    	
        for (int i=0; i<n; ++i)
            System.out.print(A[i]+" ");
    }


}
